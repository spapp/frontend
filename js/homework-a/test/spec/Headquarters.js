describe('Headquarters', function () {
    var products = ['alma', 'körte', 'szilva', 'cseresznye'];
    var runningTime = 20;
    var hq = new Headquarters(products, runningTime);

    it('function created', function () {
        expect(hq.run).toBeDefined();
        expect(hq.stop).toBeDefined();
        expect(hq.getFactory).toBeDefined();
        expect(hq.transport).toBeDefined();
        expect(hq.addFactory).toBeDefined();
        expect(Headquarters.message).toBeDefined();
    });

    it("variable factories", function () {
        expect(hq.factories).not.toBeNull();
        expect(typeof(hq.factories) === 'object').toBe(true);

        var productName = 'dinnye';
        hq.addFactory(productName);
        var factory = hq.getFactory(productName);
        expect(factory.product.name).toBe(productName);
    });

    it("variable products", function () {
        expect(Array.isArray(hq.products)).toBe(true);
        expect(hq.products).toEqual(products);
        expect(hq.products[0]).toBe(products[0]);
    });

    it("variable runningTime", function () {
        expect(hq.runningTime).toEqual(runningTime);
    });

    it("variable transportTime", function () {
        expect(hq.transportTime).toEqual(5);
    });

    it("variable transportTimeoutId", function () {
        expect(hq.transportTimeoutId).toBeNull();
    });
});