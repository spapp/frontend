describe('Factory', function () {
    var productName = 'alma';
    var capacity = 35;
    var products = {
        'körte': 3,
        'dinnye': 2,
        'eper': 5,
        'dió': 8,
        'banán': 7
    };
    var transportData = [];
    var factory = new Factory(productName, capacity);

    Object.keys(products).forEach(function (key) {
        for (var i = 0; i < products[key]; i++) {
            transportData.push(new Product(key));
        }
    });

    it('function created', function () {
        expect(factory.run).toBeDefined();
        expect(factory.stop).toBeDefined();
        expect(factory.transport).toBeDefined();
        expect(factory.getCapacity).toBeDefined();
        expect(factory.addProduct).toBeDefined();
        expect(Factory.message).toBeDefined();
    });

    it('variable timeoutIds', function () {
        expect(factory.timeoutIds).not.toBeNull();
        expect(typeof(factory.timeoutIds) === 'object').toBe(true);
    });

    it('variable storage', function () {
        expect(factory.storage).not.toBeNull();
        expect(typeof(factory.storage) === 'object').toBe(true);
        expect(Array.isArray(factory.storage[productName])).toBe(true);
        expect(factory.storage[productName].length).toBe(0);

    });

    it('variable product', function () {
        expect(factory.product.name).toBe(productName);
    });

    it('variable headquarters', function () {
        expect(factory.headquarters).toBeNull();

        factory.setHeadquarters(productName);
        expect(factory.headquarters).toBe(productName);
    });

    it('variable storageCapacity', function () {
        expect(factory.storageCapacity).toBe(capacity);
    });

    it('function getCapacity', function () {
        expect(factory.getCapacity()).toBe(factory.storageCapacity);

        Object.keys(products).forEach(function (key) {
            expect(factory.getCapacity(key)).toBe(factory.storageCapacity);
        });
    });

    it('function transport', function () {
        factory.transport(transportData);
        Object.keys(products).forEach(function (key) {
            expect(factory.storage[key].length).toBe(products[key]);
        });
    });

    it('function getCapacity (after transport)', function () {
        Object.keys(products).forEach(function (key) {
            expect(factory.getCapacity(key)).toBe(factory.storageCapacity - products[key]);
        });
    });
});