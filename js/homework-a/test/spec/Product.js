describe('Product', function () {
    var name = 'alma';
    var product = new Product(name);

    it('variable name', function () {
        expect(product.name).toBe(name);
    });
});