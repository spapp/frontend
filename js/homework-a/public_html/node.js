#!/usr/bin/env node

global.Headquarters = require(__dirname + '/js').Headquarters;

var products = ['alma', 'körte', 'szilva', 'cseresznye'];
var time = 300;
var args = process.argv.slice(2);

while (args.length) {
    switch (args.shift()) {
        case '-p':
            products = args.shift().split(',');
            break;
        case '-t':
            time = parseInt(args.shift(), 10);
            break;
        default:
            console.log();
            console.log('SYNOPSIS');
            console.log('\tnode.js [ [ -p [[[<product0>],<product0>],<productN>] ] [ -t <seconds>] ]');
            console.log('OPTIONS');
            console.log('\t-p\tcomma separated product list');
            console.log('\t-t\trunning time in seconds');
            console.log();
            process.exit();
    }
}

new Headquarters(products, time).run();
