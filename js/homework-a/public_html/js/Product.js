/**
 * Egy terméket reprezentáló objektum.
 *
 * @param aName {String}  a termék neve
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @constructor
 */
function Product(aName) {
    this.name = aName;
}
/**
 * Termék neve.
 *
 * @type {String}
 * @default null
 */
Product.prototype.name = null;

if (typeof(exports) !== 'undefined') {
    exports.Product = Product;
}