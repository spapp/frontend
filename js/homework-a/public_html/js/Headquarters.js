/**
 * A gyárakat vezérlő központ.
 *
 * @param {String[]} aProducts  termékek listája
 * @param {Number} aTime    futási idő másodpercben
 * @requires Factory
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @version 1.0.0
 * @constructor
 */
function Headquarters(aProducts, aTime) {
    this.runningTime = Math.abs(parseInt(aTime, 10));
    this.factories = {};
    this.products = aProducts;
}

if (typeof(window) === 'undefined') {
    var Factory = require(__dirname + '/Factory.js').Factory;
    exports.Headquarters = Headquarters;
}
/**
 * Transport topic neve.
 *
 * @static
 * @type {String}
 */
Headquarters.TOPIC_TRANSPORT = 'TRANSPORT';
/**
 * Process topic neve.
 *
 * @static
 * @type {String}
 */
Headquarters.TOPIC_PROCESS = 'PROCESS';
/**
 * Debug topic neve.
 *
 * @static
 * @type {String}
 */
Headquarters.TOPIC_DEBUG = 'DEBUG';
/**
 *
 * @type {String[]} termékek listája
 * @default {null}
 */
Headquarters.prototype.products = null;
/**
 *
 * @type {Object} gyárak listája
 * @default {null}
 */
Headquarters.prototype.factories = null;
/**
 * Központ futási (működési) ideje másodpercben.
 *
 * @type {Number}
 * @default 0
 */
Headquarters.prototype.runningTime = 0;
/**
 * A gyárakba történő beszállítások intervalluma másodpercben.
 *
 * @type {Number}
 * @default 5
 */
Headquarters.prototype.transportTime = 5;
/**
 * A gyárakba történő beszállítások időzítőjének id-ja.
 *
 * @type {Number}
 * @default {null}
 */
Headquarters.prototype.transportTimeoutId = null;
/**
 * A kapott üzeneteket logolja.
 *
 * @see Headquarters#TOPIC_TRANSPORT
 * @see Headquarters#TOPIC_PROCESS
 * @see Headquarters#TOPIC_DEBUG
 * @param {String} aTopic   topic neve
 * @param {String|Object} aParams   üzenet paraméterei
 * @static
 * @return {void}
 * @function
 */
Headquarters.message = function Headquarters_message(aTopic, aParams) {
    var msg = '';
    switch (aTopic) {
        case Headquarters.TOPIC_TRANSPORT:
            msg = [aTopic, aParams.from, '->', aParams.to, '(', 'count:', aParams.count, ')'].join(' ');
            break;
        case Headquarters.TOPIC_PROCESS:
            msg = [aTopic, aParams.from, 'count:', aParams.count, '(', 'capacity:', aParams.capacity, ')'].join(' ');
            break;
        case Headquarters.TOPIC_DEBUG:
            msg = [aTopic, aParams].join(' ');
            break;
    }
    console.log(msg);
};
/**
 * A központ indítás.
 *
 * @function
 * @return {void}
 */
Headquarters.prototype.run = function Headquarters_run() {
    Headquarters.message(Headquarters.TOPIC_DEBUG, 'Headquarters start.');
    // gyárak elindítása
    var l = this.products.length, i;

    for (i = 0; i < l; i++) {
        this.addFactory(this.products[i]).setHeadquarters(this).run();
    }
    // beszállítás időzítése
    this.transportTimeoutId = setTimeout(this.transport.bind(this), this.transportTime * 1000);
    // leállíás időzítése
    setTimeout(this.stop.bind(this), this.runningTime * 1000);
};
/**
 * A központ leállítása.
 *
 * @function
 * @return {void}
 */
Headquarters.prototype.stop = function Headquarters_stop() {
    var me = this, factory;
    // transport leállítása
    clearTimeout(this.transportTimeoutId);
    // gyárak leállítása
    Object.keys(me.factories).forEach(function (factoryName) {
        factory = me.getFactory(factoryName);
        if (factory) {
            factory.stop();
        }
    });

    Headquarters.message(Headquarters.TOPIC_DEBUG, 'Headquarters stopped.');
};
/**
 * Megkeresi melyik gyár dolgozza fel a terméket.
 *
 * @param {String} aName    gyár neve (feldolgozott termék neve)
 * @return {Factory}
 * @function
 */
Headquarters.prototype.getFactory = function Headquarters_getFactory(aName) {
    if (aName && aName in this.factories) {
        return this.factories[aName];
    }
    return null;
};

(function () {
    function getRandomProduct(aProducts, aMin, aMax) {
        var result = [];
        var count, i, j;
        var l = aProducts.length;

        for (i = 0; i < l; i++) {
            count = Math.floor((Math.random() * aMax) + aMin);
            for (j = aMin; j <= count; j++) {
                result.push(aProducts[i]);
            }
        }

        return result;
    }

    /**
     * Minden gyárnak minden termékből random mennyiséget szállít.
     *
     * @param {Number} aMin minimum termékszám
     * @param {Number} aMax  maximum termékszám
     * @return {void}
     * @function
     */
    Headquarters.prototype.transport = function Headquarters_transport(aMin, aMax) {
        var min = aMin || 1;
        var max = aMax || 5;
        var me = this;
        var factory = null;

        Object.keys(me.factories).forEach(function (factoryName) {
            factory = me.getFactory(factoryName);
            if (factory) {
                factory.transport(getRandomProduct(me.products, min, max));
            }
        });

        this.transportTimeoutId = setTimeout(this.transport.bind(this), this.transportTime * 1000);
    };
}());
/**
 * Létrehoz és a listához ad egy gyárat.
 *
 * @param {String} aProductName termék neve
 * @return {Factory}
 * @throws {TypeError}
 * @function
 */
Headquarters.prototype.addFactory = function Headquarters_addFactory(aProductName) {
    if (typeof(aProductName) === 'string') {
        this.factories[aProductName] = new Factory(aProductName);
    } else {
        throw new TypeError('Not supported type: ' + typeof(aProductName));
    }
    return this.factories[aProductName];
};
