/**
 * Egy gyárat reprezentáló objektum
 *
 * Egy gyár egy terméket dolgoz fel,
 * és csak bizonyos mennyiséget képes raktározni.
 *
 * @param {String} aProductName  termék neve
 * @param {String} [aCapacity] gyár kapacitása (ennyi terméket tud raktározni)
 * @requires Product
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @constructor
 */
function Factory(aProductName, aCapacity) {
    this.timeoutIds = {};
    this.storage = {};
    this.storage[aProductName] = [];
    this.product = new Product(aProductName);
    if (typeof aCapacity === 'number') {
        this.storageCapacity = parseInt(aCapacity, 10);
    }
}

if (typeof window === 'undefined') {
    var Product = require(__dirname + '/Product.js').Product;
    exports.Factory = Factory;
}
/**
 * Üzenetet küld a központnak.
 *
 * @param {String} aTopic    üzenet típusa
 * @param {String|Object} aParams   üzenet paraméterei
 * @static
 * @function
 */
Factory.message = function Factory_message(aTopic, aParams) {
    if (Headquarters && typeof Headquarters.message === 'function') {
        Headquarters.message(aTopic, aParams);
    }
};
/**
 * Az időzidett folyamatok id-ját nyilvántartó objektum.
 *
 * @type {Object}
 * @default null
 */
Factory.prototype.timeoutIds = null;
/**
 * Referencia központra.
 *
 * @type {Headquarters}
 * @default null
 */
Factory.prototype.headquarters = null;
/**
 * A gyár által feldolgozott termék.
 *
 * @type {Product}
 * @default null
 */
Factory.prototype.product = null;
/**
 * A gyár raktára.
 * A gyárba beérkező termékek nyilvántartása.
 *
 * @type {Object}
 * @default null
 */
Factory.prototype.storage = null;
/**
 * Raktár kapacitása.
 * Egy termékből mennyit képes raktározni a gyát.
 *
 * @type {Number}
 * @default 30
 */
Factory.prototype.storageCapacity = 30;
/**
 * Beállítható vele a gyár központja.
 *
 * @param {Headquarters} aHeadquarters referenciaa központra
 * @return {Factory}    this
 * @function
 */
Factory.prototype.setHeadquarters = function Factory_setHeadquarters(aHeadquarters) {
    this.headquarters = aHeadquarters;
    return this;
};

(function () {
    function iterate(aObj, aCallback) {
        Object.keys(aObj).forEach(aCallback);
    }

    /**
     * Elindítja a gyár müködését.
     *
     * @return {Factory}    this
     * @function
     */
    Factory.prototype.run = function Factory_run() {
        var process, me = this;

        iterate(this.processes, function (processName) {
            process = me.processes[processName];
            me.timeoutIds[processName] = setTimeout(process.fn.bind(me), process.time * 1000);
        });

        Factory.message(Headquarters.TOPIC_DEBUG, 'Factory (' + me.product.name + ') started.');

        return this;
    };

    /**
     * Leállítja a gyár müködését.
     *
     * @function
     * @return {Boolean}    true
     */
    Factory.prototype.stop = function Factory_stop() {
        var me = this;

        iterate(me.timeoutIds, function (processName) {
            clearTimeout(me.timeoutIds[processName]);
        });

        Factory.message(Headquarters.TOPIC_DEBUG, 'Factory (' + me.product.name + ') stoped.');

        return true;
    };

    /**
     * Termékeket beszállítását/átszállítását fogadja.
     *
     * @param {String[]} aProducts termékek listája
     * @function
     * @return {Factory}    this
     */
    Factory.prototype.transport = function Factory_transport(aProducts) {
        var l = aProducts.length;
        for (var i = 0; i < l; i++) {
            this.addProduct(aProducts[i]);
        }
        return this;
    };
    /**
     * Terméket add a raktárhoz.
     *
     * @param {String|Product} aProduct  termékneve vagy Product objektum
     * @function
     * @return {Product}    a hozzáadott termék
     */
    Factory.prototype.addProduct = function Factory_addProduct(aProduct) {
        if (typeof(aProduct) === 'string') {
            aProduct = new Product(aProduct);
        }
        if (!(aProduct.name in this.storage)) {
            this.storage[aProduct.name] = [];
        }
        if (this.getCapacity(aProduct.name) > 0) {
            this.storage[aProduct.name].push(aProduct);
        }
        return aProduct;
    };
    /**
     * A gyár egy termékre vonatkozó szabad kapacitását adja meg.
     *
     * @param {String|Product} [aProduct]   termék, vagy annak a neve
     * @function
     * @return {Number} kapacitás
     */
    Factory.prototype.getCapacity = function Factory_getCapacity(aProduct) {
        var productName = this.product.name;
        var capacity = this.storageCapacity;

        if (typeof(aProduct) === 'string') {
            productName = aProduct;
        } else if (typeof(aProduct) === 'object') {
            productName = aProduct.name;
        }

        if (Array.isArray(this.storage[productName])) {
            capacity = this.storageCapacity - this.storage[productName].length;
        }
        return capacity;
    };
    /**
     * A gyár folyamatai.
     *
     * @type {Object}
     */
    Factory.prototype.processes = {
        /**
         * A gyár saját feldolgozása.
         */
        process: {
            /**
             * A saját feldolgozás intervalluma másodpercben.
             *
             * @type {Number}
             * @default 3
             */
            time: 3,
            /**
             * A saját feldolgozás alatt feldolgozott maximális termékszám.
             *
             * @type {Number}
             * @default 10
             */
            limit: 10,
            /**
             * A saját feldolgozás folyamata.
             *
             * A feldolgozás folyamán a saját termékből a processLimit-ben meghatározzott
             * mennyiségű terméket feldolgoz. Ez után értesítést kükd a Központnak és
             * időzítia következő feldolgozást.
             *
             * @function
             * @this {Factory}
             * @return {void}
             */
            fn: function Factory_processes_process_fn() {
                var process = this.processes.process;
                var howMany = process.limit;
                if (this.storage[this.product.name].length < process.limit) {
                    howMany = this.storage[this.product.name].length;
                }
                if (howMany > 0) {
                    this.storage[this.product.name].splice(0, howMany);
                }
                Factory.message(Headquarters.TOPIC_PROCESS, {
                    from: this.product.name,
                    capacity: this.getCapacity(),
                    count: howMany
                });
                this.timeoutIds.process = setTimeout(process.fn.bind(this), process.time * 1000);
            }
        },
        /**
         * Készletek ellenőrzése.
         */
        control: {
            /**
             * Készlet ellenőrzés intervalluma másodpercben.
             *
             * @type {Number}
             * @default 10
             */
            time: 10,
            /**
             * Készlet ellenőrzési limit.
             *
             * @type {Number}
             * @default 10
             */
            limit: 20,
            /**
             * Készlet ellenőrzési folyamat.
             *
             * Ha a gyárban valamelyik, nem saját, termékből a készlet meghaladja ezt a számott,
             * akkor a központtól megkérdezi, hogy az adott terméket melyik gyár dolgozza fel,
             * attól a gyártól lekéri a szabad kapacitását, és annyi terméket átszállít oda, amennyi elfér.
             * Ez után értesítést kükd a Központnak és időzítia következő feldolgozást.
             *
             * @function
             * @this {Factory}
             * @return {void}
             */
            fn: function Factory_processes_control_fn() {
                var process = this.processes.control;
                var factory, capacity, items;
                var me = this;

                iterate(me.storage, function (productName) {
                    if (productName !== me.product.name && me.storage[productName].length > process.limit) {
                        factory = me.headquarters.getFactory(productName);
                        capacity = factory.getCapacity();
                        itemLength = 0;
                        if (capacity > 0) {
                            items = me.storage[productName].splice(0, capacity);
                            if (Array.isArray(items)) {
                                factory.transport(items);
                                Factory.message(Headquarters.TOPIC_TRANSPORT, {
                                    from: me.product.name,
                                    to: factory.product.name,
                                    count: items.length
                                });
                            }
                        }
                    }
                });

                this.timeoutIds.control = setTimeout(process.fn.bind(this), process.time * 1000);
            }
        }
    };
}());
