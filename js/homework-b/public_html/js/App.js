'use strict';
/**
 * The application object.
 *
 * There is a namespace for this application.
 *
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @version 2.0.0
 * @type {Object}
 */
var App = Object.create(Object.prototype);

(function () {
    var runningTime = 0,
        products = null,
        headquarters = null,
        producer = null,
        loadedClasses = {};

    /**
     * This function is padding the text.
     *
     * <code>
     *      pad(1, 0, 2);
     *      // return '01'
     *
     *      pad(1, 0, 2, '+');
     *      // return '+01'
     *
     *      pad(-1, 0, 2, '+');
     *      // return '-01'
     * </code>
     *
     * @function
     * @private
     * @param {String} aString original text
     * @param {String} aPad pad character
     * @param {Number} aLength the final length of the text
     * @param {String} [aDefaultPrefix=''] number prefix
     * @return {String}
     */
    function pad(aString, aPad, aLength, aDefaultPrefix) {
        var str = aString + '',
            i = str.length,
            prefix = aDefaultPrefix || '';

        if (/^\-|^\+/.test(str)) {
            prefix = str.substr(0, 1);
            str = str.substr(1);
            --i;
        }

        for (; i < aLength; i++) {
            str = '' + aPad + str;
        }
        return prefix + str;
    }

    /**
     * Replacer.
     *
     * @function
     * @private
     * @param {String} aFormat the template; contained the keys of aObject
     * @param {Object} aObject the values
     * @return {String} the formatted string
     */
    function format(aFormat, aObject) {
        var regExp = [];

        App.objectForEach(aObject, function (aKey, aValue) {
            // TODO /\./test(aKey) for subkeys -> App.Logger.Formatter.Object.format
            regExp.push(aKey);
        });

        regExp = new RegExp(regExp.join('|'), 'g');

        return aFormat.replace(regExp, function (aHit) {
            // TODO use prefix -> o['key']|o['%key']...
            return aObject[aHit];
        });
    }

    /**
     * Application initialization.
     *
     * @function
     * @private
     * @param {String[]} aProducts the prodict list
     * @param {Number} aTime running time
     * @return {void}
     */
    function initApp(aProducts, aTime) {
        var i, l;
        runningTime = Math.abs(parseInt(aTime, 10));
        products = aProducts || [];
        headquarters = App.factory('Headquarters', {
            name: 'First'
        });
        producer = App.factory('Producer', {
            topicName: headquarters.name,
            products: products
        });
        l = products.length;

        for (i = 0; i < l; i++) {
            headquarters.addFactory(products[i]);
        }
    }

    /**
     * Application's logger initialization.
     *
     * @function
     * @private
     * @return {void}
     */
    function initLogger() {
        var loggerConfig = {
            loggers: {
                console: {
                    level: App.Logger.LEVEL_DEBUG,
                    isDefault: true,
                    formatter: {
                        name: 'auto'
                    },
                    writers: ['console']
                },
                file: {
                    level: App.Logger.LEVEL_INFO,
                    formatter: {
                        name: 'object',
                        params: {
                            messageTpl: '{time(date=%FT%T%:z)} [{level}] [{topicName}-{eventName}] {message}'
                        }
                    },
                    writers: [
                        {
                            name: 'file',
                            params: {
                                fileName: '/tmp/first.log'
                            }
                        }
                    ]
                }
            }
        };

        App.Logger.setConfig(loggerConfig);
    }

    /**
     * Remove 'App.' prefix from class name and prepare parts.
     *
     * @param {String} aName the class name
     * @return {String}
     * @function
     * @private
     */
    function prepareClassName(aName) {
        var name = aName.split('.'), i = 0;

        if (name[0] === 'App') {
            name.shift();
        }

        for (; i < name.length; i++) {
            name[i] = App.ucfirst(name[i]);
        }

        return name.join('.');
    }

    /**
     * Return a class object.
     *
     * @param {String} aName class name
     * @param {mixed} aParent the parent object
     * @return {mixed}
     * @function
     * @private
     */
    function getCalss(aName, aParent) {
        var parent = aParent || App,
            name = aName.split('.'),
            currentName = App.ucfirst(name.shift()),
            currentClass = parent[currentName];

        if ((App.isObject(currentClass) || typeof currentClass === 'function') && name.length > 0) {
            currentClass = getCalss(name.join('.'), currentClass);
        }

        return currentClass;
    }

    Object.defineProperty(App, 'ucfirst', {
        writable: false,
        /**
         * Make a string's first character uppercase.
         *
         * @param {String} aText original string
         * @return {String}
         * @function
         * @private
         */
        value: function App_ucfirst(aText) {
            return aText.substr(0, 1).toUpperCase() + aText.substr(1).toLowerCase();
        }
    });

    Object.defineProperty(App, 'dateFormat', {
        writable: false,
        /**
         * Make a formatted date string.
         *
         * Interpreted sequences are:
         *      %H     hour (00..23)
         *      %M     minute (00..59)
         *      %S     second (00..60)
         *      %d     day of month (e.g., 01)
         *      %m     month (01..12)
         *      %Y     year
         *      %T     time; same as %H:%M:%S
         *      %F     full date; same as %Y-%m-%d
         *      %z     +hhmm numeric time zone (e.g., -0400)
         *      %:z    +hh:mm numeric time zone (e.g., -04:00)
         *      %r     locale's 12-hour clock time (e.g., 11:11:04 PM)
         *      %R     24-hour hour and minute; same as %H:%M
         *
         * @see date shell command
         * @param {String} aFormat date template
         * @param {Date} aDate the date
         * @return {String} formatted date string
         * @public
         * @function
         */
        value: function App_dateFormat(aFormat, aDate) {
            // TODO Date
            //  - isDate
            //  - string to date
            var date = aDate || new Date(),
                tokens = {};
            // TODO implement more tokens
            tokens['%H'] = pad(date.getHours(), '0', 2);
            tokens['%M'] = pad(date.getMinutes(), '0', 2);
            tokens['%S'] = pad(date.getSeconds(), '0', 2);
            tokens['%d'] = pad(date.getDate(), '0', 2);
            tokens['%m'] = pad(date.getMonth() + 1, '0', 2);
            tokens['%Y'] = date.getFullYear();
            tokens['%z'] = pad(Math.abs(date.getHours() - date.getUTCHours()), '0', 2, '+') + '00';
            tokens['%:z'] = pad(Math.abs(date.getHours() - date.getUTCHours()), '0', 2, '+') + ':00';
            tokens['%T'] = tokens['%H'] + ':' + tokens['%M'] + ':' + tokens['%S'];
            tokens['%F'] = tokens['%Y'] + '-' + tokens['%m'] + '-' + tokens['%d'];

            return format(aFormat, tokens);
        }
    });
    Object.defineProperty(App, 'isObject', {
        writable: false,
        /**
         * Finds whether a variable is an object.
         *
         * @param {mixed} aObject the defined variable
         * @return {Boolean} if aObject is a object
         * @public
         * @function
         */
        value: function App_isObject(aObject) {
            return (Object.prototype.toString.call(aObject) === '[object Object]');
        }
    });
    Object.defineProperty(App, 'isNodeJs', {
        /**
         * @type {Boolean}
         * @public
         */
        value: (typeof window === 'undefined'),
        writable: false
    });
    Object.defineProperty(App, 'objectForEach', {
        writable: false,
        /**
         * Executes the callback function once per object element.
         *
         * @param {Object} aObject
         * @param {Function} aCallback
         *                      - params:   {String} aKey current key
         *                                  {mixed} aValue current value
         * @function
         * @public
         */
        value: function App_objectForEach(aObject, aCallback) {
            var keys = Object.keys(aObject);
            keys.forEach(function (aKey) {
                aCallback(aKey, aObject[aKey]);
            }, aObject);
        }
    });
    Object.defineProperty(App, 'run', {
        writable: false,
        /**
         * Run the application.
         *
         * @function
         * @param {String[]} aProducts the prodict list
         * @param {Number} aTime running time
         * @return {void}
         * @public
         */
        value: function App_run(aProducts, aTime) {
            App.required('Headquarters');
            App.required('Logger');

            initLogger();
            initApp(aProducts, aTime);

            headquarters.run();
            setTimeout(App.stop, runningTime * 1000);
        }
    });

    Object.defineProperty(App, 'stop', {
        writable: false,
        /**
         * Terminate the application.
         *
         * @function
         * @public
         * @return {void}
         */
        value: function App_stop() {
            headquarters.stop();
            App.Logger.flush();
        }
    });
    Object.defineProperty(App, 'factory', {
        writable: false,
        /**
         * Create a class instance.
         *
         * @param {String} aName the class name
         * @param {mixed} aConfig the class config
         * @return {mixed} class instance
         * @function
         * @public
         */
        value: function App_factory(aName, aConfig) {
            var instance,
                classObject,
                name = prepareClassName(aName);

            classObject = App.required(name);

            if (typeof classObject === 'function') {
                instance = new classObject(aConfig);
            } else if (App.isObject(classObject)) {
                instance = classObject;
            } else {
                throw new TypeError('App.' + name + ' is not a function.');
            }

            return instance;
        }
    });

    Object.defineProperty(App, 'classExists', {
        writable: false,
        /**
         * Check the class already loaded.
         *
         * @param {String} aName the class name in App
         * @return {Boolean} true if it i9s exists
         * @public
         * @function
         */
        value: function App_classExists(aName) {
            var name = prepareClassName(aName);
            return (name in loadedClasses && loadedClasses[name] === true);
        }
    });

    Object.defineProperty(App, 'required', {
        writable: false,
        /**
         * Required module.
         *
         * Check the class already loaded.
         * If it is not loaded, it will attempt to load.
         *
         * @param {String} aName the class name in App
         * @return {mixed} the loaded class
         * @public
         * @function
         */
        value: function App_require(aName) {
            var path,
                pathName,
                classObject,
                DIRECTORY_SEPARATOR = '/',
                FILE_SUFFIX = '.js',
                preparedName = prepareClassName(aName),
                name = preparedName.split(/[\.\/\\]/);

            name = name.join(DIRECTORY_SEPARATOR) + FILE_SUFFIX;

            if (!App.classExists(aName)) {
                if (App.isNodeJs === true) {
                    path = require('path');
                    pathName = path.resolve(__dirname, 'App', name);
                    require(pathName);
                } else {
                    // TODO autoLoad in browsers
                }
            }

            classObject = getCalss(preparedName);

            loadedClasses[preparedName] = (typeof classObject === 'function' || App.isObject(classObject));

            return classObject;
        }
    });

    if (App.isNodeJs) {
        exports.App = App;
    }
}());
