'use strict';
/**
 * The observe.
 *
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @type {Object}
 */
Object.defineProperty(App, 'Observe', {
    writable: false,
    value: Object.create(Object.prototype)
});

(function () {
    var events = {},
        index = 0;

    /**
     * Generate a unique id.
     *
     * @param {String} aPrefix
     * @return {String}
     * @function
     * @private
     */
    function genId(aPrefix) {
        var prefix = aPrefix || 'event';
        return (prefix + '_' + (index++));
    }

    /**
     * The event is exists in the topic.
     *
     * @param {String} aTopicName
     * @param {String} aEventName
     * @return {Boolean}
     * @function
     * @private
     */
    function isExistingEvent(aTopicName, aEventName) {
        return (App.Observe.isExistingTopic(aTopicName) && (typeof events[aTopicName][aEventName] !== 'undefined'));
    }

    Object.defineProperty(App.Observe, 'getTopicName', {
        writable: false,
        /**
         *
         * @param {String} [aTopicName] the topic name (only prefix)
         * @return {String}
         * @function
         */
        value: function App_Observe_getTopicName(aTopicName) {
            return genId(aTopicName);
        }
    });

    Object.defineProperty(App.Observe, 'isExistingTopic', {
        writable: false,
        /**
         * Check the topic name is exists.
         *
         * @param {String} aTopicName topic name
         * @return {Boolean} true if it is exists
         * @function
         */
        value: function App_Observe_isExistingTopic(aTopicName) {
            return (typeof events[aTopicName] !== 'undefined');
        }
    });

    Object.defineProperty(App.Observe, 'subscribe', {
        writable: false,
        /**
         * Subscribe a event of topic.
         *
         * @param {String} aTopicName topic name
         * @param {String} aEventName event name
         * @param {Function} aCallback the callback function
         * @param {Object} aScope the callback function's scope
         * @return {String} event id
         * @function
         */
        value: function App_Observe_subscribe(aTopicName, aEventName, aCallback, aScope) {
            var id = genId();

            if (App.Observe.isExistingTopic(aTopicName) !== true) {
                events[aTopicName] = {};
            }
            if (isExistingEvent(aTopicName, aEventName) !== true) {
                events[aTopicName][aEventName] = {};
            }

            events[aTopicName][aEventName][id] = {
                fn: aCallback,
                scope: aScope
            };
            return id;
        }
    });
    Object.defineProperty(App.Observe, 'unsubscribe', {
        writable: false,
        /**
         * Unsubscribe a event of topic.
         *
         * @param {String} aTopicName topic name
         * @param {String} aEventName event name
         * @param {String} aEventId event id
         * @return {Boolean}
         * @function
         */
        value: function App_Observe_unsubscribe(aTopicName, aEventName, aEventId) {
            if (App.Observe.isExistingTopic(aTopicName) === true &&
                isExistingEvent(aEventName) === true &&
                typeof events[aTopicName][aEventName][aEventId] !== 'undefined') {

                delete events[aEventName][aEventId];
                return true;
            }
            return false;
        }
    });
    Object.defineProperty(App.Observe, 'fireEvent', {
        writable: false,
        /**
         * Create a event.
         *
         * @param {String} aTopicName topic name
         * @param {String} aEventName event name
         * @param {mixed} aArg
         * @param {mixed} [aArg1]
         * @param {mixed} [aArgN]
         * @function
         */
        value: function App_Observe_fireEvent(aTopicName, aEventName, aArg /* , aArg1, aArgN*/) {
            var event,
                args = [],
                i,
                l = arguments.length;

            if (l > 2) {
                for (i = 2; i < l; i++) {
                    args.push(arguments[i]);
                }
            }

            event = App.factory('Observe.Event');
            event.setEventName(aEventName);
            event.setTopicName(aTopicName);
            event.setParams(args);

            App.Logger.log(event, App.Logger.LEVEL_INFO, 'file');

            if (isExistingEvent(aTopicName, aEventName) === true) {
                App.objectForEach(events[aTopicName][aEventName], function (aEventId, aEventValue) {
                    aEventValue.fn.call(aEventValue.scope, event);
                });
            }
        }
    });
}());