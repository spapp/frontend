'use strict';
/**
 * The file writer.
 *
 * Write a message to the file.
 *
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 *
 * @param {Object} aConfig writer config
 *                      - {String} fileName the file name
 * @see App.isNodeJs
 * @constructor
 */
App.Logger.Writer.File = function App_Logger_Writer_File(aConfig) {
    this.fileName = aConfig.fileName;
    if (aConfig.flushEvent) {
        App.Observe.subscribe(aConfig.flushEvent.topicName, aConfig.flushEvent.eventName, this.flush, this);
    }
};
/**
 * The file name.
 *
 * @type {String}
 * @default {null}
 */
App.Logger.Writer.File.prototype.fileName = null;

(function () {
    var fs, os,
        buffer = {},
        bufferLimit = 100,
        maxFileSize = Math.pow(10, 7),
        logFileCount = 9;

    if (App.isNodeJs === true) {
        fs = require('fs');
        os = require('os');
    }
    /**
     * Rotate log files.
     *
     * @param {String} aFileName the file name
     * @return {void}
     * @privare
     * @function
     */
    function rotate(aFileName) {
        var i = logFileCount,
            stat,
            oldFileName,
            newFileName;

        if (fs.existsSync(aFileName)) {
            stat = fs.statSync(aFileName);
        }

        if (stat && stat.size >= maxFileSize) {
            if (fs.existsSync(aFileName + '.' + logFileCount)) {
                fs.unlinkSync(aFileName + '.' + logFileCount);
            }

            for (; i > 1; i--) {
                oldFileName = aFileName + '.' + (i - 1);
                newFileName = aFileName + '.' + i;
                if (fs.existsSync(oldFileName)) {
                    fs.renameSync(oldFileName, newFileName);
                }
            }
            fs.renameSync(aFileName, aFileName + '.1');
        }
    }

    /**
     * Write a message to the buffer.
     *
     * @param {String} aFileName the file name
     * @param {String} aMessage the message
     * @return {void}
     * @privare
     * @function
     */
    function writeToBuffer(aFileName, aMessage) {
        if (!Array.isArray(buffer[aFileName])) {
            buffer[aFileName] = [];
        }

        buffer[aFileName].push(aMessage);
    }

    /**
     * Check buffer size.
     *
     * If it equal the limit then it will be flush.
     *
     * @param aFileName
     * @return {void}
     * @privare
     * @function
     */
    function checkBuffer(aFileName) {
        if (buffer[aFileName].length >= bufferLimit) {
            flushBuffer(aFileName, bufferLimit);
        }
    }

    /**
     * Flush the buffer.
     *
     * If count is not defined then  all content will be flush.
     * Otherwise only defined count.
     *
     * @param {String} aFileName
     * @param {Number} [aCount]
     * @return {void}
     * @privare
     * @function
     */
    function flushBuffer(aFileName, aCount) {
        var data = buffer[aFileName].splice(0, (aCount || buffer[aFileName].length));
        data.push('');

        rotate(aFileName);

        fs.appendFile(aFileName, data.join(os.EOL), 'utf8');
    }

    /**
     * Write a messages to a file.
     *
     * @param {String} aMessage
     * @function
     */
    App.Logger.Writer.File.prototype.write = function App_Logger_Writer_File_write(aMessage) {
        if (App.isNodeJs === true) {
            writeToBuffer(this.fileName, aMessage);
            checkBuffer(this.fileName);
        }
    };
    /**
     * Flush the buffer.
     *
     * @function
     */
    App.Logger.Writer.File.prototype.flush = function App_Logger_Writer_File_flush() {
        if (App.isNodeJs === true) {
            flushBuffer(this.fileName);
        }
    };

}());