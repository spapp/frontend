'use strict';
/**
 * The console writer.
 *
 * Write a message to the console.
 *
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @type {Object}
 */
App.Logger.Writer.Console = Object.create(Object.prototype, {
    write: {
        writable: false,
        /**
         * Write a messages to console.
         *
         * @param {String} aMessage
         * @function
         */
        value: function App_Logger_Writer_Console_write(aMessage) {
            try {
                console.log(aMessage);
            } catch (error) {
            }
        }
    }
});
