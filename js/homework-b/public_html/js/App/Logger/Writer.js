'use strict';
/**
 * The logger writers.
 *
 * @type {Object}
 */
App.Logger.Writer = Object.create(Object.prototype);

Object.defineProperty(App.Logger.Writer, 'factory', {
    writable: false,
    /**
     * Make a logger writer.
     *
     * @param {String} aWriterName the writer name
     * @param {Object} aConfig the writer config
     * @return {mixed}
     */
    value: function App_Logger_Writer_factory(aWriterName, aConfig) {
        return App.factory('Logger.Writer.' + aWriterName, aConfig);
    }
});