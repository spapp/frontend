'use strict';
/**
 * The object formatter.
 *
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 *
 * @param {Object} aConfig formatter config
 *                      - {String} messageTpl message template
 * @constructor
 */
App.Logger.Formatter.Object = function App_Logger_Formatter_Object(aConfig) {
    if (App.isObject(aConfig) && typeof aConfig.messageTpl === 'string') {
        this.messageTpl = aConfig.messageTpl;
    }
};
/**
 * The message template.
 *
 * @type {String}
 */
App.Logger.Formatter.Object.prototype.messageTpl = '{message}';

(function () {
    // TODO use App::format
    function format(aFormat, aObject) {
        return aFormat.replace(/(\{[a-z0-9\.\(\):%=]+\})+/ig, function (aHit) {
            var key = aHit.trim().replace('{', '').replace('}', ''),
                newString = key,
                subKey, formatString;

            if (/\(/.test(key)) {
                formatString = key.split('(');
                key = formatString[0];
                formatString = formatString[1].replace(')', '').split('=');
            }
            key = key.split('.');

            if (key.length > 1) {
                subKey = key.shift();
                newString = format('{' + key.join('.') + '}', aObject[subKey]);
            } else if (typeof aObject['get' + App.ucfirst(key[0])] === 'function') {
                newString = aObject['get' + App.ucfirst(key[0])]();
            } else if (typeof aObject[key[0]] !== 'undefined') {
                if (Array.isArray(formatString)) {
                    if (formatString[0] === 'date') {
                        newString = App.dateFormat(formatString[1], aObject[key[0]]);
                    }
                } else if (key[0] === 'level') {
                    newString = App.Logger.getLevelName(aObject[key[0]]);
                } else {
                    newString = aObject[key[0]];
                }
            }
            return newString + '';
        });
    }

    /**
     * Make a message.
     *
     * @param {Object} aMessage data
     * @return {String}
     * @function
     */
    App.Logger.Formatter.Object.prototype.format = function App_Logger_Formatter_Object_format(aMessage) {
        return format(this.messageTpl, aMessage);
    };

}());