'use strict';
/**
 * The text formatter.
 *
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @type {Object}
 */
App.Logger.Formatter.Text = Object.create(Object.prototype, {
    format: {
        writable: false,
        /**
         * Format a text.
         *
         * @param {String} aMessage the message
         * @return {String}
         * @function
         */
        value: function App_Logger_Formatter_Text_format(aMessage) {
            return [aMessage, ''].join('');
        }
    }
});