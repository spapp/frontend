'use strict';
App.required('Logger.Formatter.Text');
App.required('Logger.Formatter.Object');
/**
 * The auto formatter.
 *
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @type {Object}
 */
App.Logger.Formatter.Auto = Object.create(Object.prototype, {
    format: {
        writable: false,
        /**
         * Make a message.
         *
         * @param {String} aMessage the message
         * @return {String}
         * @see App.Logger.Formatter.Text#format
         * @see App.Logger.Formatter.Object#format
         * @function
         */
        value: function App_Logger_Formatter_Auto_format(aMessage) {
            var formatter, formatterName, message = aMessage;

            if (typeof aMessage === 'string') {
                formatterName = 'text';
            } else if (App.isObject(aMessage)) {
                formatterName = 'object';
                if (typeof aMessage.getMessage === 'function') {
                    message = {
                        message: aMessage.getMessage()
                    };
                }
            }

            formatter = App.Logger.Formatter.factory(formatterName);
            return formatter.format(message);
        }
    }
});