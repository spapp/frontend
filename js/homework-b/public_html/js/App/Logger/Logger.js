'use strict';
/**
 * The factory object.
 *
 * The factory can process only one product
 * and only some are able to store.
 *
 * @param {Object} aConfig  the config
 *                  - {Object} formatter  the formatter config {@link App.Logger.Logger#setFormatter}
 *                  - {String[]|Object[]} writers the writters {@link App.Logger.Logger#addWriter}
 *                  - {Number} level displayed level {@link App.Logger}
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @constructor
 */
App.Logger.Logger = function App_Logger_Logger(aConfig) {
    var i, l;

    this.writers = [];

    this.setFormatter(aConfig.formatter.name, aConfig.formatter.params);

    if (Array.isArray(aConfig.writers)) {
        l = aConfig.writers.length;
        for (i = 0; i < l; i++) {
            if (typeof aConfig.writers[i] === 'string') {
                this.addWriter(aConfig.writers[i]);
            } else {
                this.addWriter(aConfig.writers[i].name, aConfig.writers[i].params);
            }
        }
    }

    if (typeof aConfig.level === 'number') {
        this.setDisplayedLevel(aConfig.level);
    }
};

App.required('Logger.Formatter');
App.required('Logger.Writer');

/**
 * The logger writers.
 *
 * @type {App.Logger.Writer}
 * @default {null}
 */
App.Logger.Logger.prototype.writers = null;
/**
 * The logger formatter.
 *
 * @type {App.Logger.Formatter}
 * @default {null}
 */
App.Logger.Logger.prototype.formatter = null;
/**
 * Displayed log messages level.
 *
 * @see App.Logger
 * @type {Number}
 * @default {App.Logger.LEVEL_WARN}
 */
App.Logger.Logger.prototype.displayedLevel = App.Logger.LEVEL_WARN;
/**
 * Loging a messages with all writers;
 *
 * @param {mixed} aMessage the log message
 * @param {Number} aLevel the log message level
 * @see App.Logger
 * @return {Boolean}
 * @function
 */
App.Logger.Logger.prototype.log = function App_Logger_Logger_log(aMessage, aLevel) {
    var message, l, i,
        level = App.Logger.getCleanDisplayedLevel(aLevel, this.getDisplayedLevel());

    if (level > this.getDisplayedLevel()) {
        return false;
    }

    aMessage.level = level;

    message = this.formatter.format(aMessage);
    l = this.writers.length;

    for (i = 0; i < l; i++) {
        this.writers[i].write(message);
    }
    return true;
};
/**
 * Set up the displayed level.
 *
 * @param {Number} aLevel log level
 * @function
 */
App.Logger.Logger.prototype.setDisplayedLevel = function App_Logger_Logger_setDisplayedLevel(aLevel) {
    this.displayedLevel = App.Logger.getCleanDisplayedLevel(aLevel);
};
/**
 * Get displayed level.
 *
 * @function
 * @return {Number}
 */
App.Logger.Logger.prototype.getDisplayedLevel = function App_Logger_Logger_getDisplayedLevel() {
    return this.displayedLevel;
};
/**
 * Set the logger formatter.
 *
 * @param {String} aName the formatter name
 * @param {Object} aConfig the formatter config {@link App.Logger.Formatter}
 * @see App.Logger.Formatter
 * @function
 * @throws {TypeError}
 * @return {void}
 */
App.Logger.Logger.prototype.setFormatter = function App_Logger_Logger_setFormatter(aName, aConfig) {
    if (typeof aName === 'string') {
        this.formatter = App.Logger.Formatter.factory(aName, aConfig);
    } else {
        throw new TypeError('Not supported type.');
    }
};
/**
 * Add a writer.
 *
 * @param {String} aName the writer name
 * @param {Object} aConfig the writer config
 * @see App.Logger.Writer
 * @function
 * @throws {TypeError}
 * @return {void}
 */
App.Logger.Logger.prototype.addWriter = function App_Logger_Logger_addWriter(aName, aConfig) {
    if (typeof aName === 'string') {
        this.writers.push(App.Logger.Writer.factory(aName, aConfig));
    } else {
        throw new TypeError('Not supported type.');
    }
};

/**
 * Flush the buffer.
 *
 * @function
 * @return {void}
 */
App.Logger.Logger.prototype.flush = function App_Logger_Logger_flush() {
    var i,
        l = this.writers.length;

    for (i = 0; i < l; i++) {
        if (typeof this.writers[i].flush === 'function') {
            this.writers[i].flush();
        }
    }
};
