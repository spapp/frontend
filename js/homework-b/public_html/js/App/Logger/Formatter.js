'use strict';
/**
 * The logger formatters.
 *
 * @type {Object}
 */
App.Logger.Formatter = Object.create(Object.prototype);

Object.defineProperty(App.Logger.Formatter, 'factory', {
    writable: false,
    /**
     * Make a logger formatter.
     *
     * @param {String} aFormatterName the formatter name
     * @param {Object} aConfig the formatter config
     * @return {mixed}
     */
    value: function App_Logger_Formatter_factory(aFormatterName, aConfig) {
        return App.factory('App.Logger.Formatter.' + aFormatterName, aConfig);
    }
});