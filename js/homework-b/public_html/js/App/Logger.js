'use strict';
/**
 * The application logger.
 *
 * They include all loggers and you can manage them.
 *
 * @type {Object}
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 */
App.Logger = Object.create(Object.prototype);
/**
 * required Logger.Logger
 */
App.required('Logger.Logger');

Object.defineProperty(App.Logger, 'defaultLogger', {
    writable: true,
    /**
     * The default logger name.
     *
     * @type {String}
     */
    value: null
});

Object.defineProperty(App.Logger, 'loggers', {
    writable: true,
    /**
     * They include all loggers.
     *
     * @type {Object}
     */
    value: Object.create(Object.prototype)
});

Object.defineProperty(App.Logger, 'eventIds', {
    writable: true,
    /**
     * They include id of events.
     *
     * @type {Object}
     */
    value: Object.create(Object.prototype)
});

Object.defineProperty(App.Logger, 'setConfig', {
    writable: false,
    /**
     * Configure logger.
     *
     * @param {Object} aConfig
     * @function
     * @return {void}
     */
    value: function App_Logger_setConfig(aConfig) {
        var loggers = {};

        if (typeof aConfig.loggers === 'object' && !Array.isArray(aConfig.loggers) && aConfig.loggers !== null) {
            loggers = aConfig.loggers;
        }

        App.objectForEach(loggers, function (loggerName, loggerValue) {
            App.Logger.addLogger(loggerName, loggerValue);
        });
    }

});

Object.defineProperty(App.Logger, 'addLogger', {
    writable: false,
    /**
     * Add a logger.
     *
     * @param {String} aName logger name
     * @param {Object} aConfig logger config
     * @function
     * @return {Logger}
     */
    value: function App_Logger_addLogger(aName, aConfig) {
        this.loggers[aName] = App.factory('Logger.Logger', aConfig);
        if (aConfig.isDefault === true) {
            this.defaultLogger = aName;
        }
        return this.loggers[aName];
    }
});

Object.defineProperty(App.Logger, 'removeLogger', {
    writable: false,
    /**
     * Remove a logger.
     *
     * @param {String} aName logger name
     * @function
     * @return {Logger}
     */
    value: function App_Logger_removeLogger(aName) {
        if (typeof this.loggers[aName] !== 'undefined') {
            delete this.loggers[aName];
        }
    }
});

Object.defineProperty(App.Logger, 'log', {
    writable: false,
    /**
     * Logging a message.
     *
     * @param {String|Object} aMessage
     * @param {Number} aLevel
     * @param {String} [aLoggerName]
     * @function
     * @return {void}
     */
    value: function App_Logger_log(aMessage, aLevel, aLoggerName) {
        var loggerName = aLoggerName || this.defaultLogger;
        if (typeof this.loggers[loggerName] !== 'undefined') {
            this.loggers[loggerName].log(aMessage, aLevel);
        }
    }

});
Object.defineProperty(App.Logger, 'flush', {
    writable: false,
    /**
     * Flush the buffer.
     *
     * @function
     */
    value: function App_Logger_flush() {
        App.objectForEach(this.loggers, function (loggerName, logger) {
            logger.flush();
        });
    }

});
/**
 * Emergency logger level.
 *
 * System is unusable.
 *
 * @const
 * @type {Number} LEVEL_EMERG | LEVEL_EMERGENCY
 */
/**
 * Alert logger level.
 *
 * Action must be taken immediately.
 *
 * @const
 * @type {Number} LEVEL_ALERT
 */
/**
 * Critical logger level.
 *
 * Critical conditions.
 *
 * @const
 * @type {Number} LEVEL_CRIT | LEVEL_CRITICAL
 */
/**
 * Error logger level.
 *
 * Error conditions.
 *
 * @const
 * @type {Number} LEVEL_ERR | LEVEL_ERROR
 */
/**
 * Warning logger level.
 *
 * Warning conditions
 *
 * @const
 * @type {Number} LEVEL_WARN | LEVEL_WARNING
 */
/**
 * Notice logger level.
 *
 * Normal but significant condition.
 *
 * @const
 * @type {Number} LEVEL_NOTICE
 */
/**
 * Informational logger level.
 *
 * Informational messages.
 *
 * @const
 * @type {Number} LEVEL_INFO | LEVEL_INFORMATION | LEVEL_INFORMATIONAL
 */
/**
 * Debug logger level.
 *
 * Debug messages.
 *
 * @const
 * @type {Number} LEVEL_DEBUG | LEVEL_LOG
 */
(function () {
    var levels = [
            ['EMERG', 'EMERGENCY'],
            'ALERT',
            ['CRIT', 'CRITICAL'],
            ['ERROR', 'ERR'],
            ['WARN', 'WARNING'],
            'NOTICE',
            ['INFO', 'INFORMATION', 'INFORMATIONAL'],
            ['DEBUG', 'LOG']
        ],
        levelPrefix = 'LEVEL_',
        defaultLevel = 4,
        level,
        i, j;

    for (i = 0; i < levels.length; i++) {
        if (Array.isArray(levels[i])) {
            level = levels[i];
        } else {
            level = [levels[i]];
        }
        for (j = 0; j < level.length; j++) {
            Object.defineProperty(App.Logger, levelPrefix + level[j], {
                value: i,
                enumerable: true,
                writable: false
            });
        }
    }

    Object.defineProperty(App.Logger, 'getCleanDisplayedLevel', {
        writable: false,
        /**
         * Get a valid loglevel.
         *
         * @param {Number} aLevel log level
         * @param {Number} aDefaultLevel default value
         * @return {Number} valid log level
         * @function
         */
        value: function App_Logger_getCleanDisplayedLevel(aLevel, aDefaultLevel) {
            var nextLevel = parseInt(aLevel, 10);
            if (typeof nextLevel !== 'number') {
                nextLevel = (typeof aDefaultLevel === 'number') ? aDefaultLevel : defaultLevel;
            }
            if (nextLevel < 0) {
                nextLevel = 0;
            } else if (nextLevel >= levels.length) {
                nextLevel = levels.length - 1;
            }
            return nextLevel;
        }
    });
    Object.defineProperty(App.Logger, 'getLevelName', {
        writable: false,
        /**
         * Get a log level name.
         *
         * @param {Number} aLevel log level
         * @return {String} log level name
         * @function
         */
        value: function App_Logger_getLevelName(aLevel) {
            var name = levels[aLevel];
            if (Array.isArray(levels[aLevel])) {
                name = levels[aLevel][0];
            }
            return name;
        }
    });
}());
