'use strict';
/**
 * The producer object.
 *
 * @param {Object} aConfig {@link App.Producer#products}, {@link App.Producer#topicName}
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @constructor
 */
App.Producer = function App_Producer(aConfig) {
    try {
        this.products = aConfig.products;
        this.topicName = aConfig.topicName;
    } catch (error) {
        throw new TypeError('Required: products, topicName');
    }

    App.Observe.subscribe(this.topicName, App.Headquarters.EVENT_RUN, this.run, this);
};
/**
 * Run event name.
 *
 * @type {String}
 */
App.Producer.EVENT_RUN = 'App_Producer_run';
/**
 * Stop event name.
 *
 * @type {String}
 */
App.Producer.EVENT_STOP = 'App_Producer_stop';
/**
 * Transport event name.
 *
 * @type {String}
 */
App.Producer.EVENT_TRANSPORT = 'App_Producer_transport';
/**
 * Transport timeout id.
 *
 * @type {Number}
 * @default {null}
 */
App.Producer.prototype.transportTimeoutId = null;
/**
 * Product list.
 *
 * @type {String[]}
 * @default {null}
 */
App.Producer.prototype.products = null;
/**
 * Topic name.
 *
 * @type {String}
 * @default {null}
 */
App.Producer.prototype.topicName = null;
/**
 * Run the producer.
 *
 * @function
 */
App.Producer.prototype.run = function App_Producer_run() {
    var message = 'The Producer started.';
    App.Logger.log(message, App.Logger.LEVEL_DEBUG);
    App.Producer.transportStart.call(this, this);
    App.Observe.subscribe(this.topicName, App.Headquarters.EVENT_STOP, this.stop, this);
    App.Observe.fireEvent(this.topicName, App.Producer.EVENT_RUN, message);
};
/**
 * Terminate producer.
 *
 * @function
 */
App.Producer.prototype.stop = function App_Producer_stop() {
    var message = 'The Producer stopped.';
    App.Logger.log(message, App.Logger.LEVEL_DEBUG);
    clearTimeout(this.transportTimeoutId);
    App.Observe.fireEvent(this.topicName, App.Producer.EVENT_STOP, message);
};
(function () {
    var min = 1,
        max = 5,
        time = 5;

    function getRandomProduct(aProducts, aMin, aMax) {
        var result = [];
        var count, i, j;
        var l = aProducts.length;

        for (i = 0; i < l; i++) {
            count = Math.floor((Math.random() * aMax) + aMin);
            for (j = aMin; j <= count; j++) {
                result.push(aProducts[i]);
            }
        }

        return result;
    }

    function transport(aScope) {
        var message,
            transportedProducts,
            products = aScope.products;

        products.forEach(function (productName) {
            transportedProducts = getRandomProduct(products, min, max);
            message = 'Producer transported ' + transportedProducts.length + ' product to "' + productName + '" factory.';
            // fire a sub transport event
            App.Observe.fireEvent(aScope.topicName, App.Producer.EVENT_TRANSPORT + '.' + productName, {
                message: message,
                products: transportedProducts
            });
            // fire the transport event
            App.Observe.fireEvent(aScope.topicName, App.Producer.EVENT_TRANSPORT, {
                message: message,
                productName: productName,
                products: transportedProducts
            });

            App.Logger.log(message, App.Logger.LEVEL_DEBUG);
        });

        App.Producer.transportStart.call(aScope, aScope);
    }

    Object.defineProperty(App.Producer, 'transportStart', {
        writable: false,
        /**
         * Start a new transport event.
         *
         * @param aScope
         * @function
         */
        value: function App_Producer_transportStart(aScope) {
            aScope.transportTimeoutId = setTimeout(transport.bind(aScope, aScope), time * 1000);
        }
    });
}());