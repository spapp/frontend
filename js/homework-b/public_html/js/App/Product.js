'use strict';
/**
 * The product object.
 *
 * @param {String} aName  the product name
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @constructor
 */
App.Product = function App_Product(aName) {
    this.name = aName;
};
/**
 * The product name.
 *
 * @type {String}
 * @default null
 */
App.Product.prototype.name = null;
