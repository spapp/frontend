'use strict';
App.required('Product');
/**
 * The factory object.
 *
 * The factory can process only one product
 * and only some are able to store.
 *
 * @param {Object} aConfig  the config
 *                  - {String} productName  the product name
 *                  - {Number} [capacity] capacity of factory (many products can be stored)
 *                  - {String} topicName the topic name
 * @requires App.Factory
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @constructor
 */
App.Factory = function App_Factory(aConfig) {
    var productName, storageCapacity = this.storageCapacity;
    this.timeoutIds = {};
    this.storage = {};

    if (typeof aConfig === 'string' && aConfig.length > 0) {
        productName = aConfig;
    } else {
        try {
            if (typeof aConfig.productName === 'string') {
                productName = aConfig.productName;
            }
            if (typeof aConfig.capacity === 'number') {
                storageCapacity = parseInt(aConfig.capacity, 10);
            }
            if (typeof aConfig.topicName === 'string') {
                this.topicName = aConfig.topicName;
            }
        } catch (error) {
            throw error;
        }
    }

    if (productName) {
        this.storage[productName] = [];
        this.product = App.factory('Product', productName);
    } else {
        throw new TypeError('Required: productName');
    }
    if (typeof storageCapacity === 'number') {
        this.storageCapacity = parseInt(storageCapacity, 10);
    }

    App.Observe.subscribe(this.topicName, App.Headquarters.EVENT_RUN, this.run, this);
};
/**
 * Run event name.
 *
 * @type {String}
 * @static
 */
App.Factory.EVENT_RUN = 'App_Factory_run';
/**
 * Stop event name.
 *
 * @type {String}
 * @static
 */
App.Factory.EVENT_STOP = 'App_Factory_stop';
/**
 * Process event name.
 *
 * @type {String}
 * @static
 */
App.Factory.EVENT_PROCESS = 'App_Factory_process';
/**
 * Transport event name.
 *
 * @type {String}
 * @static
 */
App.Factory.EVENT_TRANSPORT = 'App_Factory_transport';
/**
 * The topic name.
 *
 * @type {String}
 * @default null
 */
App.Factory.prototype.topicName = null;
/**
 * The timed process IDs.
 *
 * @type {Object}
 * @default null
 */
App.Factory.prototype.timeoutIds = null;
/**
 * The  processed product.
 *
 * @type {Product}
 * @default null
 */
App.Factory.prototype.product = null;
/**
 * The factory warehouse.
 *
 * @type {Object}
 * @default null
 */
App.Factory.prototype.storage = null;
/**
 * The storage capacity (per product).
 *
 * @type {Number}
 * @default 30
 */
App.Factory.prototype.storageCapacity = 30;
/**
 * Run the factory.
 *
 * @return {Factory}    this
 * @function
 */
App.Factory.prototype.run = function App_Factory_run() {
    var me = this, message = 'Factory (' + this.product.name + ') started.';

    App.Logger.log(message, App.Logger.LEVEL_DEBUG);

    App.objectForEach(App.Factory.processes, function (processName, processValue) {
        processValue.call(me, me);
    });

    App.Observe.subscribe(this.topicName, App.Headquarters.EVENT_STOP, this.stop, this);
    App.Observe.subscribe(this.topicName, App.Factory.EVENT_TRANSPORT, function (aEvent) {
        if (aEvent.params[0].productName === me.product.name) {
            this.transport.call(this, aEvent.params[0].products.splice(0, this.getCapacity()));
        }
    }, this);

    App.Observe.subscribe(this.topicName, App.Producer.EVENT_TRANSPORT + '.' + this.product.name, function (aEvent) {
        this.transport.call(this, aEvent.params[0].products);
    }, this);

    App.Observe.fireEvent(this.topicName, App.Factory.EVENT_RUN, message);

    return this;
};
/**
 * Terminate the factory.
 *
 * @function
 * @return {Boolean}    true
 */
App.Factory.prototype.stop = function App_Factory_stop() {
    var me = this,
        message = 'Factory (' + this.product.name + ') stopped.';

    App.Logger.log(message, App.Logger.LEVEL_DEBUG);

    App.objectForEach(App.Factory.processes, function (processName) {
        clearTimeout(me.timeoutIds[processName]);
    });

    App.Observe.fireEvent(this.topicName, App.Factory.EVENT_STOP, message);

    return true;
};
/**
 * Transport manager.
 *
 * @param {String[]} aProducts the product list
 * @function
 * @return {Factory}    this
 */
App.Factory.prototype.transport = function App_Factory_transport(aProducts) {
    var l, i;

    if (Array.isArray(aProducts)) {
        l = aProducts.length;
        for (i = 0; i < l; i++) {
            this.addProduct(aProducts[i]);
        }
    }
    return this;
};
/**
 * Add a product to the store.
 *
 * @param {String|Product} aProduct
 * @function
 * @return {Product}    added product
 */
App.Factory.prototype.addProduct = function App_Factory_addProduct(aProduct) {
    var product = aProduct;
    if (typeof product === 'string') {
        product = App.factory('Product', product);
    }

    if (typeof this.storage[product.name] === 'undefined') {
        this.storage[product.name] = [];
    }
    if (this.getCapacity(product) > 0) {
        this.storage[product.name].push(product);
    }
    return product;
};
/**
 * The store capacity (per product).
 *
 * @param {String|Product} [aProduct]   product or product name
 * @function
 * @return {Number} capacity
 */
App.Factory.prototype.getCapacity = function App_Factory_getCapacity(aProduct) {
    return this.storageCapacity - this.getProductCount(aProduct);
};
/**
 * Count a product in the store.
 *
 * @param {String|Product} [aProduct]   product object or product name
 * @return {Number} product count
 * @function
 */
App.Factory.prototype.getProductCount = function App_Factory_getProductCount(aProduct) {
    var productName = this.product.name,
        count = 0;

    if (typeof aProduct === 'string') {
        productName = aProduct;
    } else if (aProduct) {
        try {
            productName = aProduct.name;
        } catch (err) {
            throw err;
        }
    }

    if (Array.isArray(this.storage[productName])) {
        count = this.storage[productName].length;
    }
    return count;
};
/**
 * The factory's processes.
 *
 * @type {Object}
 */
App.Factory.processes = {};
/**
 * The factory's production process.
 *
 * There is run every three seconds and the processed is ten products.
 *
 * @event App_Factory_process
 *          - {String}  message  the event message
 *          - {Number}  capacity    the factory capacity
 *          - {String}  productName  product name
 *          - {Number}  count   processed products count
 * @function
 * @return void
 */
(function () {
    var time = 3,
        limit = 10;

    function fn(aScope) {
        var howMany = limit,
            message,
            count = aScope.getProductCount(aScope.product.name);


        if (count < limit) {
            howMany = count;
        }
        if (howMany > 0) {
            aScope.storage[aScope.product.name].splice(0, howMany);
        }
        message = 'The factory processed ' + howMany + ' ' + aScope.product.name + '(s).';
        App.Observe.fireEvent(aScope.topicName, App.Factory.EVENT_PROCESS, {
            message: message,
            capacity: aScope.getCapacity(),
            productName: aScope.product.name,
            count: howMany
        });

        App.Logger.log(message, App.Logger.LEVEL_DEBUG);

        start.apply(aScope, [aScope]);
    }

    function start(aScope) {
        aScope.timeoutIds.process = setTimeout(fn.bind(aScope, aScope), time * 1000);
    }

    Object.defineProperty(App.Factory.processes, 'process', {
        value: start,
        enumerable: true,
        writable: false
    });
}());
/**
 * The factory's production process.
 *
 * There is run every ten seconds and the check the stores.
 *
 * @event App_Factory_transport
 *          - {String}  message  the event message
 *          - {String}  from factory's name
 *          - {String}  productName transported product name
 *          - {Array}byRef  products transported products
 * @function
 * @return void
 */
(function () {
    var time = 10,
        limit = 20;

    function fn(aScope) {
        var message;
        App.objectForEach(aScope.storage, function (productName) {
            var productCount = aScope.getProductCount(productName);
            if (productName !== aScope.product.name && productCount > limit) {
                message = 'The "' + aScope.product.name + '" factory transport ' + productName + '.';
                App.Observe.fireEvent(aScope.topicName, App.Factory.EVENT_TRANSPORT, {
                    message: message,
                    from: aScope.product.name,
                    productName: productName,
                    products: aScope.storage[productName]
                });
                App.Logger.log(message, App.Logger.LEVEL_DEBUG);
            }
        });
        start.apply(aScope, [aScope]);
    }

    function start(aScope) {
        aScope.timeoutIds.control = setTimeout(fn.bind(aScope, aScope), time * 1000);
    }

    Object.defineProperty(App.Factory.processes, 'control', {
        value: start,
        enumerable: true,
        writable: false
    });
}());