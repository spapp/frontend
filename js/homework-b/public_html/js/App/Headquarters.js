'use strict';
App.required('Observe');
App.required('Logger');
App.required('Factory');
/**
 * The headquarters object.
 *
 * This is manage the factories.
 *
 * @param {Object} aConfig  the config
 *                  - {String} name  the headquarters name
 * @requires App.Headquarters
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 * @constructor
 */
App.Headquarters = function App_Headquarters(aConfig) {
    this.factories = {};

    if (App.Observe.isExistingTopic(aConfig.name)) {
        this.name = App.Observe.getTopicName(aConfig.name);
    } else {
        this.name = aConfig.name;
    }
};
/**
 * Run event name.
 *
 * @type {String}
 * @const
 */
App.Headquarters.EVENT_RUN = 'App_Headquarters_run';
/**
 * Stop event name.
 *
 * @type {String}
 * @const
 */
App.Headquarters.EVENT_STOP = 'App_Headquarters_stop';
/**
 * The headquarters name.
 *
 * @type {String}
 * @default {null}
 */
App.Headquarters.prototype.name = null;
/**
 * The factories.
 *
 * @type {Object} foctory list
 * @default {null}
 */
App.Headquarters.prototype.factories = null;
/**
 * Make a factory.
 *
 * @param {String} aProductName product name
 * @return {Factory} the new factory
 * @throws {TypeError}
 * @function
 */
App.Headquarters.prototype.addFactory = function App_Headquarters_addFactory(aProductName) {
    if (typeof aProductName === 'string') {
        this.factories[aProductName] = App.factory('Factory', {
            productName: aProductName,
            topicName: this.name
        });
    } else {
        throw new TypeError('Not supported type: ' + typeof aProductName);
    }
    return this.factories[aProductName];
};
/**
 * Run the headquarters.
 *
 * @function
 * @return {void}
 */
App.Headquarters.prototype.run = function App_Headquarters_run() {
    var message = 'Headquarters (' + this.name + ') started.';
    App.Logger.log(message, App.Logger.LEVEL_DEBUG);
    App.Observe.fireEvent(this.name, App.Headquarters.EVENT_RUN, message);
    App.Observe.subscribe(this.name, 'Factory_process', function (aParams) {
        // do sometings
    }, this);
};
/**
 * Terminate the headquarters.
 *
 * @function
 * @return {void}
 */
App.Headquarters.prototype.stop = function App_Headquarters_stop() {
    var message = 'Headquarters stopped.';
    App.Logger.log(message, App.Logger.LEVEL_DEBUG);
    App.Observe.fireEvent(this.name, App.Headquarters.EVENT_STOP, message);
};
/**
 * Get a factory.
 *
 * @param {String} aName    factory name
 * @return {Factory}
 * @function
 */
App.Headquarters.prototype.getFactory = function App_Headquarters_getFactory(aName) {
    if (typeof this.factories[aName] !== 'undefined') {
        return this.factories[aName];
    }
    return null;
};