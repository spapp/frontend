'use strict';
/**
 * The event object.
 *
 * @author <a href="mailto:spapp@spappsite.hu">Sandor Papp</a>
 *
 * @param {String} [aTopicName] topic name
 * @constructor
 */
App.Observe.Event = function App_Observe_Event(aTopicName) {
    this.time = new Date();
    this.setParams([]);
    this.setTopicName(aTopicName);
};
/**
 * Event time.
 *
 * @type {Date}
 * @default {null}
 */
App.Observe.Event.prototype.time = null;
/**
 * Event name.
 *
 * @type {String}
 * @default {null}
 */
App.Observe.Event.prototype.eventName = null;
/**
 * Topic name.
 *
 * @type {String}
 * @default {null}
 */
App.Observe.Event.prototype.topicName = null;
/**
 * Some parameters in event.
 *
 * @type {mixed[]}
 * @default {Array}
 */
App.Observe.Event.prototype.params = null;
/**
 * Get a message string.
 *
 * @return {String}
 * @function
 */
App.Observe.Event.prototype.getMessage = function App_Observe_Event_getMessage() {
    var message;
    if (Array.isArray(this.params)) {
        if (typeof this.params[0] === 'string') {
            message = this.params[0];
        } else if (App.isObject(this.params[0]) && typeof this.params[0].message === 'string') {
            message = this.params[0].message;
        }
    }
    return message || '';
};
/**
 * Set up topic name.
 *
 * @param {String} aName
 * @function
 */
App.Observe.Event.prototype.setTopicName = function App_Observe_Event_setTopicName(aName) {
    this.topicName = aName;
};
/**
 * Set up event name.
 *
 * @param {String} aName
 * @function
 */
App.Observe.Event.prototype.setEventName = function App_Observe_Event_setEventName(aName) {
    this.eventName = aName;
};
/**
 * Set up params.
 *
 * @param {mixed[]} aParams
 * @function
 */
App.Observe.Event.prototype.setParams = function App_Observe_Event_setParams(aParams) {
    this.params = aParams;
};
