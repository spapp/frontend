describe('App.Headquarters', function () {
    var name = 'hq',
        headquarters = App.factory('Headquarters', name);

    it('App.Headquarters is created.', function () {
        expect(App.Headquarters).toBeDefined();
    });

    it('App.Headquarters\'s functions created.', function () {
        expect(headquarters.run).toBeDefined();
        expect(headquarters.stop).toBeDefined();
        expect(headquarters.addFactory).toBeDefined();
        expect(headquarters.getFactory).toBeDefined();
    });
});