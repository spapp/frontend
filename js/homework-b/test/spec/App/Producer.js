describe('App.Producer', function () {

    it('App.Producer is created.', function () {
        expect(App.Producer).toBeDefined();
    });

    it('App.Producer\'s functions created.', function () {
        var producer = App.factory('Producer', {
            products: ['apple'],
            topicName: 'test'
        });
        expect(producer.run).toBeDefined();
        expect(producer.stop).toBeDefined();

        expect(App.Producer.transportStart).toBeDefined();
    });

});