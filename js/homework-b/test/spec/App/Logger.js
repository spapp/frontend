describe('App.Logger', function () {

    it('App.Logger is created.', function () {
        expect(App.Logger).toBeDefined();
    });

    it('App.Logger\'s functions created.', function () {
        expect(App.Logger.setConfig).toBeDefined();
        expect(App.Logger.addLogger).toBeDefined();
        expect(App.Logger.removeLogger).toBeDefined();
        expect(App.Logger.log).toBeDefined();
        expect(App.Logger.getCleanDisplayedLevel).toBeDefined();
        expect(App.Logger.getLevelName).toBeDefined();
    });

    describe('App.Logger.Writer', function () {
        it('App.Logger.Writer is created.', function () {
            expect(App.Logger.Writer).toBeDefined();
        });
        it('App.Logger.Writer\'s functions created.', function () {
            expect(App.Logger.Writer.factory).toBeDefined();
        });

        describe('App.Logger.Writer.Console', function () {
            it('App.Logger.Writer.Console is created.', function () {
                expect(App.Logger.Writer.Console).toBeDefined();
            });
            it('App.Logger.Writer.Console\'s functions created.', function () {
                expect(App.Logger.Writer.Console.write).toBeDefined();
            });
        });
        describe('App.Logger.Writer.File', function () {
            it('App.Logger.Writer.File is created.', function () {
                expect(App.Logger.Writer.File).toBeDefined();
            });
        });
    });

    describe('App.Logger.Formatter', function () {
        it('App.Logger.Formatter is created.', function () {
            expect(App.Logger.Formatter).toBeDefined();
        });
        it('App.Logger.Formatter\'s functions created.', function () {
            expect(App.Logger.Formatter.factory).toBeDefined();
        });

        describe('App.Logger.Formatter.Text', function () {
            it('App.Logger.Formatter.Text is created.', function () {
                expect(App.Logger.Formatter.Text).toBeDefined();
            });
            it('App.Logger.Formatter.Text\'s functions created.', function () {
                expect(App.Logger.Formatter.Text.format).toBeDefined();
            });
        });

        describe('App.Logger.Formatter.Object', function () {
            it('App.Logger.Formatter.Object is created.', function () {
                expect(App.Logger.Formatter.Object).toBeDefined();
            });
        });

        describe('App.Logger.Formatter.Auto', function () {
            it('App.Logger.Formatter.Auto is created.', function () {
                expect(App.Logger.Formatter.Auto).toBeDefined();
            });
            it('App.Logger.Formatter.Auto\'s functions created.', function () {
                expect(App.Logger.Formatter.Auto.format).toBeDefined();
            });
        });
    });
    describe('App.Logger.Logger', function () {
        var config = {
                level: App.Logger.LEVEL_DEBUG,
                isDefault: true,
                formatter: {
                    name: 'Auto'
                },
                writers: ['Console']
            },
            logger = App.factory('Logger.Logger', config);

        it('App.Logger.Logger is created.', function () {
            expect(App.Logger.Logger).toBeDefined();
        });
        it('App.Logger.Logger\'s functions created.', function () {
            expect(logger.log).toBeDefined();
            expect(logger.setDisplayedLevel).toBeDefined();
            expect(logger.getDisplayedLevel).toBeDefined();
            expect(logger.setFormatter).toBeDefined();
            expect(logger.addWriter).toBeDefined();
        });
    });
})
;