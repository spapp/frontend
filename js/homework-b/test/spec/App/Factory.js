describe('App.Factory', function () {
    var productName = 'apple',
        factory = App.factory('Factory', productName);

    it('App.Factory is created.', function () {
        expect(App.Factory).toBeDefined();
    });

    it('App.Factory\'s functions created.', function () {
        expect(factory.run).toBeDefined();
        expect(factory.stop).toBeDefined();
        expect(factory.transport).toBeDefined();
        expect(factory.addProduct).toBeDefined();
        expect(factory.getCapacity).toBeDefined();
        expect(factory.getProductCount).toBeDefined();

        expect(App.Factory.processes).toBeDefined();
        expect(App.Factory.processes.process).toBeDefined();
        expect(App.Factory.processes.control).toBeDefined();
    });
});