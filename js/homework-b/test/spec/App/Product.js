describe('App.Product', function () {
    it('App.Product is created.', function () {
        expect(App.Product).toBeDefined();
    });

    it('App.Product variable name', function () {
        var name = 'apple',
            product = App.factory('Product', name);

        expect(product.name).toBe(name);
    });
});