describe('App.Observe', function () {

    it('App.Observe is created.', function () {
        expect(App.Observe).toBeDefined();
    });
    it('App.Observe\'s functions created.', function () {
        expect(App.Observe.getTopicName).toBeDefined();
        expect(App.Observe.isExistingTopic).toBeDefined();
        expect(App.Observe.subscribe).toBeDefined();
        expect(App.Observe.unsubscribe).toBeDefined();
        expect(App.Observe.fireEvent).toBeDefined();
    });

    describe('App.Observe.Event', function () {
        var event = App.factory('App.Observe.Event');

        it('App.Observe.Event is created.', function () {
            expect(App.Observe.Event).toBeDefined();
        });
        it('App.Observe.Event\'s functions created.', function () {
            expect(event.getMessage).toBeDefined();
            expect(event.setTopicName).toBeDefined();
            expect(event.setEventName).toBeDefined();
            expect(event.setParams).toBeDefined();
        });
    });
});