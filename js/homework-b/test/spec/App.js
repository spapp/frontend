describe('App', function () {

    it('App is created.', function () {
        expect(App).toBeDefined();
    });

    it('App\'s functions created.', function () {
        expect(App.run).toBeDefined();
        expect(App.stop).toBeDefined();
        expect(App.factory).toBeDefined();
        expect(App.dateFormat).toBeDefined();
        expect(App.isObject).toBeDefined();
        expect(App.isNodeJs).toBeDefined();
        expect(App.objectForEach).toBeDefined();
    });
    it('App.dateFormat', function () {
        var mysqlDateTimeFormat0 = '%Y-%m-%d %H:%M:%S',
            mysqlDateTimeFormat1 = '%F %T',
            mysqlDate = '2012-11-04',
            mysqlTime = '12:21:32',
            mysqlDateTime = mysqlDate + ' ' + mysqlTime,
            date = new Date(2012, 10, 4, 12, 21, 32, 0);
        expect(App.dateFormat(mysqlDateTimeFormat0, date)).toBe(mysqlDateTime);
        expect(App.dateFormat(mysqlDateTimeFormat1, date)).toBe(mysqlDateTime);
    });
    it('App.isObject', function () {
        var undefinedVariable;
        var Fn = function Fn() {
        };
        var fn = new Fn();

        expect(App.isObject({})).toBe(true);
        expect(App.isObject(fn)).toBe(true);

        expect(App.isObject('s')).toBe(false);
        expect(App.isObject([])).toBe(false);
        expect(App.isObject(null)).toBe(false);
        expect(App.isObject(undefinedVariable)).toBe(false);
        expect(App.isObject(15)).toBe(false);
        expect(App.isObject(Fn)).toBe(false);
    });
    it('App.isNodeJs', function () {
        expect(App.isNodeJs).toBe(false);
    });
    it('App.objectForEach', function () {
        var obj = {a: 1, b: 2, c: 'c'},
            objKeys = [],
            objValues = [],
            keys = ['a', 'b', 'c'],
            values = [1, 2, 'c'];

        App.objectForEach(obj, function (aKey, aValue) {
            objKeys.push(aKey);
            objValues.push(aValue);
        });

        expect(objKeys.join(',')).toEqual(keys.join(','));
        expect(objValues.join(',')).toEqual(values.join(','));
    });


});